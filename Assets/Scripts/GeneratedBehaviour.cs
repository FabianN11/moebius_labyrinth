﻿using UnityEngine;
using System.Collections;

public class GeneratedBehaviour : MonoBehaviour {

	static public bool moveGenN = false;
	static public bool moveGenE = false;
	static public bool moveGenS = false;
	static public bool moveGenW = false;

	private bool ready;

	// Use this for initialization
	void Start () {
		ready = false;

		moveGenN = false;
		moveGenE = false;
		moveGenS = false;
		moveGenW = false;

		StartCoroutine(Time(0.5f));
	}
	
	// Update is called once per frame
	void Update () {
		if (ready) {
			if (moveGenN) {
				transform.position += new Vector3 (0f, 0f, -12.5f); 
				moveGenN = false;
			}
			if (moveGenE) {
				transform.position += new Vector3 (-12.5f, 0f, 0f);
				moveGenE = false;
			}
			if (moveGenS) {
				transform.position += new Vector3 (0f, 0f, 12.5f);
				moveGenS = false;
			}
			if (moveGenW) {
				transform.position += new Vector3 (12.5f, 0f, 0f);
				moveGenW = false;
			}
		}
	}

	void OnTriggerEnter (Collider coll) {
		if (coll.tag == "deadzone") {
			Destroy (gameObject);
			Generator.spawnCount--;
		}
	}

	IEnumerator Time(float wait)
	{
		yield return new WaitForSeconds(wait);
		ready = true;
	}
}
