﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour {
	public float speed;

	static public bool PosN = false;
	static public bool PosE = false;
	static public bool PosS = false;
	static public bool PosW = false;

	static public int CountN = 0;
	static public int CountE = 0;
	static public int CountS = 0;
	static public int CountW = 0;

	private Rigidbody rb;
	
	void Start () {
		rb = GetComponent<Rigidbody> ();

		if (!Application.isEditor) {
			Cursor.visible = false; 
		}

		PosN = false;
		PosE = false;
		PosS = false;
		PosW = false;

		CountN = 0;
		CountE = 0;
		CountS = 0;
		CountW = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown (KeyCode.R))&&(Application.isEditor)) {
			SceneManager.LoadScene (0);
			//Application.LoadLevel(0);
		}
		
		if ((Input.GetKeyDown (KeyCode.Escape))) {
			Application.Quit();
		}
		/* old movement
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		if (moveVertical > 0) {
			transform.Translate (moveVertical * Vector3.forward * speed * Time.deltaTime);
		}
		
		if (moveVertical < 0) {
			transform.Translate (moveVertical * Vector3.forward * speed * Time.deltaTime);
		}
		
		if (moveHorizontal < 0) {
			transform.Translate (-moveHorizontal * Vector3.left * speed * Time.deltaTime);
		}
		
		if (moveHorizontal > 0) {
			transform.Translate (moveHorizontal * -Vector3.left * speed * Time.deltaTime);
		}*/
		
		if (PosN) {
			transform.position += new Vector3 (0f, 0f, -12.5f); 
			PosN = false;
		}
		if (PosE) {
			transform.position += new Vector3 (-12.5f, 0f, 0f);
			PosE = false;
		}
		if (PosS) {
			transform.position += new Vector3 (0f, 0f, 12.5f);
			PosS = false;
		}
		if (PosW) {
			transform.position += new Vector3 (12.5f, 0f, 0f);
			PosW = false;
		}
	}

	void OnTriggerEnter (Collider coll) {
		if (coll.tag == "deadzone") {
			SceneManager.LoadScene (0);
			//Application.LoadLevel(0);
		}
	}

	void FixedUpdate () {
		Vector3 tx = new Vector3(0,0,0);
		Vector3 tz = new Vector3(0,0,0);

		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
			
		if (moveVertical > 0) {
			tz += (moveVertical * transform.forward * speed * 100 * Time.deltaTime);
		}
		
		if (moveVertical < 0) {
			tz += (moveVertical * transform.forward * speed * 100 * Time.deltaTime);
		}
		
		if (moveHorizontal < 0) {
			tx += (-moveHorizontal * -transform.right * speed * 100 * Time.deltaTime);
		}
		
		if (moveHorizontal > 0) {
			tx += (moveHorizontal * transform.right * speed * 100 * Time.deltaTime);
		}

		rb.velocity = tx + tz;
	}

	static public void Win () {
		//Win Screen here
		Application.Quit();
	}
}
