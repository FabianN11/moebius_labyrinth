﻿using UnityEngine;
using System.Collections;

public class FogScript : MonoBehaviour {
	[SerializeField]private bool FogChange = true;
	static public float R;
	static public float G;
	static public float B;
	[SerializeField]private float darkFac = 0.99f;
	[SerializeField]private float negdarkFac;
	[SerializeField]private float densityFac = 1.0075f;
	[SerializeField]private int changeChance = 4;
	static public bool FChange = false;

	// Use this for initialization
	void Start () {
		R = 1.0f;
		G = 1.0f;
		B = 1.0f;
		negdarkFac = 1.0f/darkFac;
	}

	// Update is called once per frame
	void Update () {
		if (FChange == true) {
			Change ();
			FChange = false;
		}
	}

	void Change () {
		if (FogChange) {
			//fog color & density
			int rFac = UnityEngine.Random.Range (0, changeChance);
			int gFac = UnityEngine.Random.Range (0, changeChance);
			int bFac = UnityEngine.Random.Range (0, changeChance);

			if (rFac < changeChance - 1) {
				R *= darkFac;
			}
			if (rFac >= changeChance - 1) {
				R *= negdarkFac;
			}

			if (gFac < changeChance - 1) {
				G *= darkFac;
			}			
			if (gFac >= changeChance - 1) {
				G *= negdarkFac;
			}

			if (bFac < changeChance - 1) {
				B *= darkFac;
			}
			if (bFac >= changeChance - 1) {
				B *= negdarkFac;
			}

			RenderSettings.fogColor = new Color (R, G, B);
			RenderSettings.fogDensity *= densityFac;

			Debug.Log ("R " + R + " G " + G + " B " + B);
			Debug.Log ("Density: " + RenderSettings.fogDensity);
		}
	}
}
