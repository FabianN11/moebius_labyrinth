﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PathGen : MonoBehaviour {

	private string[] pathSequence;
	public int minR = 5;
	public int maxR = 10;
	private int r;
	private string thisString;
	private string sequence;
	static public string path;
	public Text Test;

	// Use this for initialization
	void Start () {
		path = "";

		r = Random.Range(minR, maxR);

		pathSequence = new string[r];

		for (int i = 0; i < r; i++) {


			int rS = Random.Range (0, 4);
			Debug.Log ("rS" + rS);

			if (rS == 0) { //N
				if ((i > 0) && (pathSequence [i - 1] == "S")) {
					int nrS = Random.Range (0, 3);
					Debug.Log ("nrS" + nrS);
					if (nrS == 0) {
						thisString = "S";
					}
					if (nrS == 1) {
						thisString = "E";
					}
					if (nrS == 2) {
						thisString = "W";
					}
				} else {
					thisString = "N";
				}
			}

			if (rS == 1) { //E
				if ((i > 0) && (pathSequence [i - 1] == "W")) {
					int erS = Random.Range (0, 3);
					Debug.Log ("erS" + erS);
					if (erS == 0) {
						thisString = "N";
					}
					if (erS == 1) {
						thisString = "W";
					}
					if (erS == 2) {
						thisString = "S";
					}
				} else {
					thisString = "E";
				}
			}

			if (rS == 2) { //S
				if ((i > 0) && (pathSequence [i - 1] == "N")) {
					int srS = Random.Range (0, 3);
					Debug.Log ("srS" + srS);
					if (srS == 0) {
						thisString = "N";
					}
					if (srS == 1) {
						thisString = "E";
					}
					if (srS == 2) {
						thisString = "W";
					}
				} else {
					thisString = "S";
				}
			}

			if (rS == 3) { //W
				if ((i > 0) && (pathSequence [i - 1] == "E")) {
					int wrS = Random.Range (0, 3);
					Debug.Log ("wrS" + wrS);
					if (wrS == 0) {
						thisString = "N";
					}
					if (wrS == 1) {
						thisString = "S";
					}
					if (wrS == 2) {
						thisString = "E";
					}
				} else {
					thisString = "W";
				}
			}


			pathSequence [i] = thisString;
			sequence += pathSequence [i];
		}
		Debug.Log (sequence);
		Test.text = sequence;
	}

	// Update is called once per frame
	void Update () {
		if (path.Contains (sequence)) {
			Controller.Win();
		}
	}

	static public void addToPath(string thisDirection) {
		path += thisDirection;
		Debug.Log (path);
	}
}