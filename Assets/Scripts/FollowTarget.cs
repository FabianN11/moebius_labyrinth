﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {
	public Transform target;
	public Vector3 offset = new Vector3(0f, 7.5f, 0f);
	private bool rotate = true;
	public bool rotationDelay = true;
	private float rotationStep = 0.0f;
	private Quaternion previousTargetRotation;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void LateUpdate() {


		if (rotate) {
			Quaternion targetRotation;
			if (rotationDelay) {
				if (previousTargetRotation != target.transform.rotation) {
					previousTargetRotation = target.transform.rotation;
					rotationStep = 0.05f;
					//Debug.Log("Reset camera target rotation);
				}

				targetRotation = Quaternion.Slerp (transform.rotation,
				                                   target.transform.rotation,
				                                   rotationStep);
				if (rotationStep < 1.0) {
					rotationStep += 0.001f;
				}
			} else {
				targetRotation = target.rotation;
			}
			Vector3 updateRot = targetRotation * offset;
			transform.position = target.position + updateRot;
			transform.rotation = targetRotation;
		} else {
			transform.position = target.position + offset;
		}
	}

}
