using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour {
	[SerializeField] private bool X = false;
	[SerializeField] private bool Y = false;
	[SerializeField] private bool neg = false;

	static public int roomCount = 0;
	
	private bool active = false;
	private bool spawnYes = false;

	// Use this for initialization
	void Start () {
		roomCount = 0;
	}
	
	// Update is called once per frame
	void Update () {

		if (active) {
			//N
			if ((!X) && (Y) && (!neg)) {
				Controller.CountN++;
				//Controller.CountE = 0;
				//Controller.CountS = 0;
				//Controller.CountW = 0;
				Controller.PosN = true;
				GeneratedBehaviour.moveGenN = true;
				PathGen.addToPath ("N");
				Debug.Log ("N"+Controller.CountN + " E"+Controller.CountE + " S"+Controller.CountS + " W"+Controller.CountW);
				active = false;
				if (spawnYes) {
					Generator.SpawnN = true;
					spawnYes = false;
				}
			}

			//E
			if ((X) && (!Y) && (!neg)) {
				//Controller.CountN = 0;
				Controller.CountE++;
				//Controller.CountS = 0;
				//Controller.CountW = 0;
				Controller.PosE = true;
				GeneratedBehaviour.moveGenE = true;
				PathGen.addToPath ("E");
				Debug.Log ("N"+Controller.CountN + " E"+Controller.CountE + " S"+Controller.CountS + " W"+Controller.CountW);
				active = false;
				if (spawnYes) {
					Generator.SpawnE = true;
					spawnYes = false;
				}
			}

			//S
			if ((!X) && (Y) && (neg)) {
				//Controller.CountN = 0;		
				//Controller.CountE = 0;		
				Controller.CountS++;
				//Controller.CountW = 0;
				Controller.PosS = true;
				GeneratedBehaviour.moveGenS = true;
				PathGen.addToPath ("S");
				Debug.Log ("N"+Controller.CountN + " E"+Controller.CountE + " S"+Controller.CountS + " W"+Controller.CountW);
				active = false;
				if (spawnYes) {
					Generator.SpawnE = true;
					spawnYes = false;
				}
			}

			//W
			if ((X) && (!Y) && (neg)) {
				//Controller.CountN = 0;	
				//Controller.CountE = 0;
				//Controller.CountS = 0;
				Controller.CountW++;
				Controller.PosW = true;
				GeneratedBehaviour.moveGenW = true;
				PathGen.addToPath ("W");
				Debug.Log ("N"+Controller.CountN + " E"+Controller.CountE + " S"+Controller.CountS + " W"+Controller.CountW);
				active = false;
				if (spawnYes) {
					Generator.SpawnW = true;
					spawnYes = false;
				}
			}
		}

		/*if ((Controller.CountN > 99)||
		    (Controller.CountE > 99)||
		    (Controller.CountS > 99)||
		    (Controller.CountW > 99)) {
			Application.LoadLevel(0);
		}*/
	
	}

	void OnTriggerEnter(Collider coll) {
		if (coll.tag == "Player") {
			int spawnchance = UnityEngine.Random.Range (0, 5);
			Debug.Log (spawnchance);

			if (spawnchance > 3) {
				spawnYes = true;
			}

			active = true;

			roomCount++;
			Debug.Log ("Rooms: " + roomCount);

			FogScript.FChange = true;
		}
	}
}
