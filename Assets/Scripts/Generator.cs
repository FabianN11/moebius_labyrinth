﻿using UnityEngine;
using System.Collections;

public class Generator : MonoBehaviour {

	public AudioClip audio1;
	public AudioClip audio2;
	public AudioClip audio3;
	public AudioClip audio4;
	public AudioClip audio5;

	private AudioClip thisaudio;

	public Transform prefab1;
	public Transform prefab2;

	private Transform thisprefab;

	static public bool SpawnN = false;
	static public bool SpawnE = false;
	static public bool SpawnS = false;
	static public bool SpawnW = false;

	private int spawnMax = 1;
	private Transform[] spawns;
	private bool ready = true;

	static public int spawnCount = 0;
	
	private Vector3 NN;
	private Vector3 NE;
	private Vector3 EE;
	private Vector3 ES;
	private Vector3 SS;
	private Vector3 SW;
	private Vector3 WW;
	private Vector3 WN;
	private float roomSize = (12.5f * 0.5f) -1.25f;
	private float spawnHeight = 0.6f;

	private Vector3 spawnVec;


	// Use this for initialization
	void Start () {
		spawns = new Transform[spawnMax];
		ready = true;

		//Rooms
		NN = new Vector3 (0, 0, 25);
		NE = new Vector3 (12.5f, 0, 12.5f);
		EE = new Vector3 (25, 0, 0);
		ES = new Vector3(12.5f, 0, -12.5f);
		SS = new Vector3(0, 0, -25);
		SW = new Vector3(-12.5f, 0, -12.5f);
		WW = new Vector3(-25, 0, 0);
		WN = new Vector3(-12.5f, 0, 12.5f);

	}
	
	// Update is called once per frame
	void Update()
	{
		if (SpawnN) {
			int nr = Random.Range (1, 6);		
			switch (nr) {
				
			case 1:
				//RoomNN
				spawnVec = new Vector3(UnityEngine.Random.Range (NN.x-roomSize,NN.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (NN.z-roomSize,NN.z+roomSize));
				break;
				
			case 2:
				//RoomNE
				spawnVec = new Vector3(UnityEngine.Random.Range (NE.x-roomSize,NE.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (NE.z-roomSize,NE.z+roomSize));
				break;
				
			case 3:
				//RoomEE
				spawnVec = new Vector3(UnityEngine.Random.Range (EE.x-roomSize,EE.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (EE.z-roomSize,EE.z+roomSize));
				break;
				
			case 4:
				//RoomWW
				spawnVec = new Vector3(UnityEngine.Random.Range (WW.x-roomSize,WW.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (WW.z-roomSize,WW.z+roomSize));
				break;
				
			case 5:
				//RoomWN
				spawnVec = new Vector3(UnityEngine.Random.Range (WN.x-roomSize,WN.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (WN.z-roomSize,WN.z+roomSize));;
				break;
				
			default:
				print ("Oups. Something went wrong :(");
				break;
			}
			spawn ();
			SpawnN = false;
		}

		if (SpawnE) {
			int er = Random.Range (1, 6);		
			switch (er) {
				
			case 1:
				//RoomNN
				spawnVec = new Vector3(UnityEngine.Random.Range (NN.x-roomSize,NN.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (NN.z-roomSize,NN.z+roomSize));
				break;
				
			case 2:
				//RoomNE
				spawnVec = new Vector3(UnityEngine.Random.Range (NE.x-roomSize,NE.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (NE.z-roomSize,NE.z+roomSize));
				break;
				
			case 3:
				//RoomEE
				spawnVec = new Vector3(UnityEngine.Random.Range (EE.x-roomSize,EE.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (EE.z-roomSize,EE.z+roomSize));
				break;
				
			case 4:
				//RoomES
				spawnVec = new Vector3(UnityEngine.Random.Range (ES.x-roomSize,ES.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (ES.z-roomSize,ES.z+roomSize));
				break;
				
			case 5:
				//RoomSS
				spawnVec = new Vector3(UnityEngine.Random.Range (SS.x-roomSize,SS.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (SS.z-roomSize,SS.z+roomSize));
				break;
							
			default:
				print ("Oups. Something went wrong :(");
				break;
			}
			spawn ();
			SpawnE = false;
		}

		if (SpawnS) {
			int sr = Random.Range (1, 6);	
			switch (sr) {
				
			case 1:
				//RoomEE
				spawnVec = new Vector3(UnityEngine.Random.Range (EE.x-roomSize,EE.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (EE.z-roomSize,EE.z+roomSize));
				break;
				
			case 2:
				//RoomES
				spawnVec = new Vector3(UnityEngine.Random.Range (ES.x-roomSize,ES.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (ES.z-roomSize,ES.z+roomSize));
				break;
				
			case 3:
				//RoomSS
				spawnVec = new Vector3(UnityEngine.Random.Range (SS.x-roomSize,SS.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (SS.z-roomSize,SS.z+roomSize));
				break;
				
			case 4:
				//RoomSW
				spawnVec = new Vector3(UnityEngine.Random.Range (SW.x-roomSize,SW.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (SW.z-roomSize,SW.z+roomSize));
				break;
				
			case 5:
				//RoomWW
				spawnVec = new Vector3(UnityEngine.Random.Range (WW.x-roomSize,WW.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (WW.z-roomSize,WW.z+roomSize));
				break;
				
			default:
				print ("Oups. Something went wrong :(");
				break;
			}
			spawn ();
			SpawnS = false;
		}

		if (SpawnW) {
			int wr = Random.Range (1, 6);	
			switch (wr) {
				
			case 1:
				//RoomNN
				spawnVec = new Vector3(UnityEngine.Random.Range (NN.x-roomSize,NN.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (NN.z-roomSize,NN.z+roomSize));
				break;
				
			case 2:
				//RoomSS
				spawnVec = new Vector3(UnityEngine.Random.Range (SS.x-roomSize,SS.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (SS.z-roomSize,SS.z+roomSize));
				break;
				
			case 3:
				//RoomSW
				spawnVec = new Vector3(UnityEngine.Random.Range (SW.x-roomSize,SW.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (SW.z-roomSize,SW.z+roomSize));
				break;
				
			case 4:
				//RoomWW
				spawnVec = new Vector3(UnityEngine.Random.Range (WW.x-roomSize,WW.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (WW.z-roomSize,WW.z+roomSize));
				break;
				
			case 5:
				//RoomWN
				spawnVec = new Vector3(UnityEngine.Random.Range (WN.x-roomSize,WN.x+roomSize),
				                       spawnHeight,
				                       UnityEngine.Random.Range (WN.z-roomSize,WN.z+roomSize));
				break;
				
			default:
				print ("Oups. Something went wrong :(");
				break;
			}
			spawn ();
			SpawnW = false;
		}
	}

	void spawn() {

		if ((ready) && (spawnCount < spawnMax)) {
			int ar = Random.Range (1, 6);	
			switch (ar) {

			case 1:
				thisaudio = audio1;
				break;
		
			case 2:
				thisaudio = audio2;
				break;
		
			case 3:
				thisaudio = audio3;
				break;
		
			case 4:
				thisaudio = audio4;
				break;
		
			case 5:
				thisaudio = audio5;
				break;
		
			default:
				print ("Oups. Something went wrong :(");
				break;
			}

			int pr = Random.Range (1, 3);	
			switch (pr) {
			
			case 1:
				thisprefab = prefab1;
				break;
			
			case 2:
				thisprefab = prefab2;
				break;
			
			default:
				print ("Oups. Something went wrong :(");
				break;
			}

			for (int i = 0; i < spawnMax; i++) {

				spawns[i] = (Transform)Instantiate(thisprefab, spawnVec, Quaternion.identity);
				thisprefab.GetComponent<AudioSource>().clip = thisaudio;
				spawnCount++;
				ready = false;
				StartCoroutine(Time(0.5f));
			}
		}
	}

	IEnumerator Time(float wait)
	{
		yield return new WaitForSeconds(wait);
		ready = true;
	}

}
